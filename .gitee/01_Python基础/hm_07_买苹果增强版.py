"""
需求
收银员输入苹果的价格，单价：元/斤
收银员输入用户购买苹果的重量，单位：斤
计算并且输出付款金额
"""
price_str = input("请输入苹果单价：")
# price = float(input("请输入苹果单价："))
weight_str = input("请输入苹果的重量：")

price = float(price_str)
weight = float(weight_str)
# 两个字符串变量之间不可以用乘法
#money = float(price_str) * float(weight_str)
money = price * weight
print(money)