"""
需求
1.定义字符串变量name，输出 我的名字叫小明，请多多关照！
2.定义整数变量student_no,输出 我的学号是000001
3.定义小数price、weight、money，输出苹果单价9.00元/斤，购买了5.00斤，需要支付45.00元
4.定义一个小数scale，输出数据比例是10.00%
"""
name = "小明"

print("我的名字叫%s,请多多关照！" % name)

student_no = 1

print("我的学号是%06d" % student_no)

price = 9.00

weight = 5.00

money = price * weight

print("苹果单价%.02f元/斤，购买了%.02f斤，需要支付%.02f元" % (price,weight,money))

scale = 10.00

print("数据比例是%.02f%%" % scale)